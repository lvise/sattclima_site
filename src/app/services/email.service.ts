import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  private apiUrl = environment.apiUrl;

  constructor(
    private httpClient: HttpClient
  ) { }

  sendMail(mailObj) {
    let route = `${this.apiUrl}/email`;
    return this.httpClient.post(route, mailObj, { responseType: 'text' });
  }
}
