import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home/home.component';
import { SliderComponent } from './shared/slider/slider/slider.component';
import { HeaderComponent } from './shared/header/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './shared/footer/footer.component';
import { SuporteComponent } from './modules/suporte/suporte.component';
import { RepresentanteComponent } from './modules/representante/representante.component';
import { FormContatoComponent } from './shared/form-contato/form-contato.component';
import { PerguntasFrequentesComponent } from './shared/perguntas-frequentes/perguntas-frequentes.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MapaSuporteComponent } from './modules/suporte/components/mapa-suporte/mapa-suporte.component';
import { FormRepresentanteComponent } from './modules/representante/components/form-representante/form-representante.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SliderComponent,
    HeaderComponent,
    FooterComponent,
    SuporteComponent,
    RepresentanteComponent,
    FormContatoComponent,
    PerguntasFrequentesComponent,
    MapaSuporteComponent,
    FormRepresentanteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    GooglePlaceModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDP4mEdmtqYVCNupz0PXobAcBm1tEUXRvY'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
