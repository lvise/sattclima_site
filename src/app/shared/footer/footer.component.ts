import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public mobile: boolean = screen.width < 768 ? true : false;

  constructor() { }

  ngOnInit(): void {
  }

  goToSatturno() {
    // window.location.href = 'http://www.satturno.com.br/';
    window.open('http://www.satturno.com.br/');
  }

}
