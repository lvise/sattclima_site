import { Component, OnInit } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-perguntas-frequentes',
  templateUrl: './perguntas-frequentes.component.html',
  styleUrls: ['./perguntas-frequentes.component.scss']
})
export class PerguntasFrequentesComponent implements OnInit {
  faChevronDown = faChevronDown;
  constructor() { }

  ngOnInit(): void {
  }

}
