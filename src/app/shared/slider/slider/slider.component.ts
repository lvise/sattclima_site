import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  public mobile: boolean = screen.width < 768 ? true : false;

  images = [
    'assets/img/dashboard-carrossel.png',
    'assets/img/app-carrossel.png',
    'assets/img/smart-carrossel.png'];

  constructor() { }

  ngOnInit(): void {
  }

}
