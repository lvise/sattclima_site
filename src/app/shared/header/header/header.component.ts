import { Component, OnInit, Output, Input } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public faChevronDown = faChevronDown;
  public mobile: boolean = screen.width < 768 ? true : false;
  public dropdownOpen: boolean = false;
  public pages: Array<any> = ['home', 'suporte', 'representante'];
  @Input('page') actualPage;

  constructor() { }

  ngOnInit(): void {
    console.log(this.actualPage)
  }

  toggleDropdown() {
    this.dropdownOpen = !this.dropdownOpen;
  }

  scrollFunc() {
    let el = document.getElementById("section-contato");
    el.scrollIntoView();
    this.dropdownOpen = false;
  }

}
