import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailService } from 'src/app/services/email.service';

@Component({
  selector: 'app-form-contato',
  templateUrl: './form-contato.component.html',
  styleUrls: ['./form-contato.component.scss']
})
export class FormContatoComponent implements OnInit {
  public loading: boolean = false;
  public formContato: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private emailService: EmailService
  ) { }

  ngOnInit(): void {
    this.createFormContato();
  }

  createFormContato() {
    this.formContato = this.formBuilder.group({
      nome: ['', Validators.required],
      email: ['', Validators.required, Validators.email],
      mensagem: ['', Validators.required]
    })
  }

  submitFormContato() {
    this.loading = true;
    let mailObj = {
      from: this.formContato.controls.email.value,
      to: "lvise.batista@gmail.com",
      subject: "formulário de contato",
      text: this.formContato.controls.mensagem.value
    }

    this.emailService.sendMail(mailObj).subscribe(
      (msg) => {
        this.loading = false;
        alert(msg)
      }, (err) => {
        this.loading = false;
        console.log(err)
      }
    );
  }

}
