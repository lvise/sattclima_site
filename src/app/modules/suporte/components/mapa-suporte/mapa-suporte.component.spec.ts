import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaSuporteComponent } from './mapa-suporte.component';

describe('MapaSuporteComponent', () => {
  let component: MapaSuporteComponent;
  let fixture: ComponentFixture<MapaSuporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaSuporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaSuporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
