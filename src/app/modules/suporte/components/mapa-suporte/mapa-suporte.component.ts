import { Component, OnInit, ViewChild } from '@angular/core';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';

@Component({
  selector: 'app-mapa-suporte',
  templateUrl: './mapa-suporte.component.html',
  styleUrls: ['./mapa-suporte.component.scss']
})
export class MapaSuporteComponent implements OnInit {

  public lat: number = -23.5031298;
  public lng: number = -47.5160192;
  public zoom: number = 15;
  public previous;
  public formattedAddress: string;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;

  public options: any = {
    types: [],
    componentRestrictions: { country: 'BR' }
  };

  public markers: marker[] = [
    {
      lat: -23.5031298,
      lng: -47.5160192,
      label: '1',
      draggable: false,
      info: 'Tantto LTDA'
    },

  ]

  constructor() { }

  ngOnInit(): void {
  }

  public handleAddressChange(address: any) {
    // console.log(address)
    this.formattedAddress = address.formatted_address;
    this.lat = address.geometry.location.lat()
    this.lng = address.geometry.location.lng()
    console.log(address)
    // Do some stuff
  }

  clickedMarker(infowindow) {
    if (this.previous) {
      console.log('teste')
      this.previous.close();
    }
    console.log('teste1')
    this.previous = infowindow;
  }

}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  info: string;
}
