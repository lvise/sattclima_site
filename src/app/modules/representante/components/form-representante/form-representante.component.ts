import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmailService } from 'src/app/services/email.service';

@Component({
  selector: 'app-form-representante',
  templateUrl: './form-representante.component.html',
  styleUrls: ['./form-representante.component.scss']
})
export class FormRepresentanteComponent implements OnInit {
  public loading: boolean = false;
  public formRepresentante: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private emailService: EmailService
  ) { }

  ngOnInit(): void {
    this.createFormRepresentante();
  }

  createFormRepresentante() {
    this.formRepresentante = this.formBuilder.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      telefone: ['', Validators.required],
      produto: ['', Validators.required],
      mensagem: ['', Validators.required]
    })
  }

  submitFormRepresentante() {
    this.loading = true;
    let mailObj = {
      from: this.formRepresentante.controls.email.value,
      to: "lvise.batista@gmail.com",
      subject: "formulário de representante",
      text: this.formRepresentante.controls.mensagem.value
    }

    this.emailService.sendMail(mailObj).subscribe(
      (msg) => {
        this.loading = false;
        alert(msg)
      }, (err) => {
        this.loading = false;
        console.log(err)
      }
    );
  }

}
